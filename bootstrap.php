<?php

// Polyfill para intl
if (!extension_loaded('intl')) {
    $path = __DIR__ . DIRECTORY_SEPARATOR . 'global';
    $classes = [
        'Locale',
        'MessageFormatter',
        'IntlDateFormatter',
        'NumberFormatter'
    ];
    foreach ($classes as $class) {
        require_once "{$path}/{$class}.php";
    }
}