<?php

namespace Pyansa\Validation;

use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Factory;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Registra los providers requeridos para el funcionamiento de este
     *
     * @return void
     */
    protected function registerRequiredProviders()
    {
        $this->app->register('Pyansa\Translation\TranslationServiceProvider');
    }

    /**
     * Carga los mensajes default de los validators
     *
     * @return void
     */
    protected function loadDefaultMessages()
    {
        $filesystem = $this->app['files'];
        $translator = $this->app['translator'];
        $messages = $filesystem->getRequire(__DIR__ . DIRECTORY_SEPARATOR . "messages.php");
        $translator->merge('*', 'validation', $translator->getLocale(), $messages);
    }

    /**
     * Registra el factory de los validators
     *
     * @return void
     */
    protected function registerFactory()
    {
        $this->app->instance('validator', new Factory($this->app['translator']));
    }

    /**
     * Sobreescritura de Illuminate\Support\ServiceProvider::register
     * Registra el service provider
     *
     * @return void
     */
    public function register()
    {
        $this->registerRequiredProviders();
        $this->loadDefaultMessages();
        $this->registerFactory();
    }
}
