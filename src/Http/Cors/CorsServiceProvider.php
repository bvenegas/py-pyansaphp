<?php

namespace Pyansa\Http\Cors;

use Illuminate\Support\ServiceProvider;
use Cake\Core\Configure;
use Cake\Routing\DispatcherFactory;
use Pyansa\Http\Middleware\HandleCors;

class CorsServiceProvider extends ServiceProvider
{
    /**
     * Registra la instancia de CorsService
     *
     * @return void
     */
    protected function registerCorsService()
    {
        $config = Configure::read('Cors');
        if (!isset($config)) {
            $config = [];
        }
        $this->app->instance('cors', new CorsService($config));
    }

    /**
     * Registra el middleware (filter) para manejar Cors
     *
     * @return void
     */
    protected function registerCorsMiddleware()
    {
        DispatcherFactory::add(new HandleCors());
    }

    /**
     * Sobreescritura de Illuminate\Support\ServiceProvider::register
     * Registra el service provider
     *
     * @return void
     */
    public function register()
    {
        $this->registerCorsService();
        $this->registerCorsMiddleware();
    }
}
