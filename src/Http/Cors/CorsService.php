<?php

namespace Pyansa\Http\Cors;

use Cake\Network\Request;
use Cake\Network\Response;

class CorsService
{
    /**
     * Configuracion
     *
     * @var array
     */
    protected $config;

    /**
     * Constructor de la clase
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = $this->normalizeConfig($config);
    }

    /**
     * Normaliza la configuracion para poder usarla dentro del servicio
     *
     * @param  array  $config
     * @return array
     */
    protected function normalizeConfig(array $config = [])
    {
        $config += [
            'allowedOrigins' => [],
            'allowedOriginsPatterns' => [],
            'supportsCredentials' => false,
            'allowedHeaders' => [],
            'exposedHeaders' => [],
            'allowedMethods' => [],
            'maxAge' => 0
        ];

        // normaliza '*' a true
        if (in_array('*', $config['allowedOrigins'])) {
            $config['allowedOrigins'] = true;
        }
        if (in_array('*', $config['allowedHeaders'])) {
            $config['allowedHeaders'] = true;
        } else {
            $config['allowedHeaders'] = array_map('strtolower', $config['allowedHeaders']);
        }
        if (in_array('*', $config['allowedMethods'])) {
            $config['allowedMethods'] = true;
        } else {
            $config['allowedMethods'] = array_map('strtoupper', $config['allowedMethods']);
        }

        return $config;
    }

    /**
     * Verifica si el header `Origin` del request es el mismo desde donde se origina el request.
     *
     * @param  Cake\Network\Request $request
     * @return boolean
     */
    protected function isSameHost(Request $request)
    {
        $origin = $request->header('Origin');
        $schemeAndHost = $request->scheme() . '://' . $request->host();

        return $origin === $schemeAndHost;
    }

    /**
     * Verifica si el method del request esta permitido
     *
     * @param  Cake\Network\Request $request
     * @return boolean
     */
    protected function checkMethod(Request $request)
    {
        if ($this->config['allowedMethods'] === true) {
            return true;
        }
        $requestMethod = strtoupper($request->header('Access-Control-Request-Method'));

        return in_array($requestMethod, $this->config['allowedMethods']);
    }

    /**
     * Verifica si el origin del request esta permitido
     *
     * @param  Cake\Network\Request $request
     * @return boolean
     */
    protected function checkOrigin(Request $request)
    {
        if ($this->config['allowedOrigins'] === true) {
            return true;
        }
        $origin = $request->header('Origin');
        if (in_array($origin, $this->config['allowedOrigins'])) {
            return true;
        }
        foreach ($this->config['allowedOriginsPatterns'] as $pattern) {
            if (preg_match($pattern, $origin)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Prepara el response al agregar los headers correspondientes de un response preflight
     *
     * @param  Cake\Network\Request  $request
     * @param  Cake\Network\Response $response
     * @return Cake\Network\Response
     */
    protected function preparePreflightResponse(Request $request, Response $response)
    {
        if ($this->config['supportsCredentials']) {
            $response->header('Access-Control-Allow-Credentials', 'true');
        }

        $response->header('Access-Control-Allow-Origin', $request->header('Origin'));

        if ($this->config['maxAge']) {
            $response->header('Access-Control-Max-Age', $this->config['maxAge']);
        }

        $allowMethods = $this->config['allowedMethods'] === true ?
            strtoupper($request->header('Access-Control-Request-Method')) :
            implode(', ', $this->config['allowedMethods']);
        $response->header('Access-Control-Allow-Methods', $allowMethods);

        $allowHeaders = $this->config['allowedHeaders'] === true ?
            strtoupper($request->header('Access-Control-Request-Headers')) :
            implode(', ', $this->config['allowedHeaders']);
        $response->header('Access-Control-Allow-Headers', $allowHeaders);

        return $response;
    }

    /**
     * Maneja un request preflight
     *
     * @param  Cake\Network\Request $request
     * @return Cake\Network\Response
     */
    public function handlePreflightRequest(Request $request, Response $response)
    {
        if (!$this->checkOrigin($request)) {
            $response->body('Origin not allowed');
            $response->statusCode(403);

            return $response;
        }

        if (!$this->checkMethod($request)) {
            $response->body('Method not allowed');
            $response->statusCode(405);

            return $response;
        }

        $requestHeaders = [];
        if ($this->config['allowedHeaders'] !== true && $request->header('Access-Control-Request-Headers') !== null) {
            // en caso que allowedHeaders no sea true se deben verificar los headers
            $headers = strtolower($request->header('Access-Control-Request-Headers'));
            $requestHeaders = array_filter(explode(',', $headers));
            foreach ($requestHeaders as $header) {
                if (!in_array(trim($header), $this->config['allowedHeaders'])) {
                    $response->body('Header not allowed');
                    $response->statusCode(403);

                    return $response;
                }
            }
        }

        return $this->preparePreflightResponse($request, $response);
    }

    /**
     * Maneja un request prohibido
     *
     * @param  Cake\Network\Request $request
     * @return Cake\Network\Response
     */
    public function handleForbiddenRequest(Request $request, Response $response)
    {
        $response->body('Not allowed in CORS policy');
        $response->statusCode(403);

        return $response;
    }

    /**
     * Maneja un request permitido al preparar el response agregando los headers correspondientes a un response CORS
     *
     * @param  Cake\Network\Request  $request
     * @param  Cake\Network\Response $response
     * @return Cake\Network\Response
     */
    public function handleAllowedRequest(Request $request, Response $response)
    {
        if (!$this->checkOrigin($request)) {
            return $response;
        }

        $headers = $response->header();
        $response->header('Access-Control-Allow-Origin', $request->header('Origin'));
        if (!isset($headers['Vary'])) {
            $response->header('Vary', 'Origin');
        } else {
            $response->header('Vary', $headers['Vary'] . ', Origin');
        }

        if ($this->config['supportsCredentials']) {
            $response->header('Access-Control-Allow-Credentials', 'true');
        }

        if ($this->config['exposedHeaders']) {
            $response->header('Access-Control-Expose-Headers', implode(', ', $this->config['exposedHeaders']));
        }

        return $response;
    }

    /**
     * Verifica si el request proporcionada es CORS
     *
     * @param  Cake\Network\Request $request
     * @return boolean
     */
    public function isCorsRequest(Request $request)
    {
        return $request->header('Origin') !== null && !$this->isSameHost($request);
    }

    /**
     * Verifica si el request proporcionado es preflight
     *
     * @param  Cake\Network\Request $request
     * @return boolean
     */
    public function isPreflightRequest(Request $request)
    {
        return $this->isCorsRequest($request) &&
            $request->method() === 'OPTIONS' &&
            $request->header('Access-Control-Request-Method') !== null;
    }

    /**
     * Verifica si el request proporcionado es permitido de acuerdo a los origines permitidos.
     *
     * @param  Cake\Network\Request $request
     * @return boolean
     */
    public function isRequestAllowed(Request $request)
    {
        return $this->checkOrigin($request);
    }
}
