<?php

namespace Pyansa\Http\Middleware;

use Cake\Routing\DispatcherFilter;
use Cake\Event\Event;
use Pyansa\Support\Facades\Request;
use Pyansa\Support\Facades\Response;

class SetDispatchedRequestResponse extends DispatcherFilter
{
    /**
     * Aplicacion
     *
     * @var Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * Constructor de la clase
     *
     * @param Illuminate\Contracts\Foundation\Application $app
     */
    public function __construct($app)
    {
        parent::__construct();
        $this->app = $app;
    }

    /**
     * Sobreescritura de Cake\Routing\DispatcherFilter::beforeDispatch
     *
     * @param Cake\Event\Event $event
     * @return void
     */
    public function beforeDispatch(Event $event)
    {
        $request = $event->data['request'];
        $response = $event->data['response'];

        // asigna las instancias a la aplicacion
        $this->app->instance('request', $request);
        $this->app->instance('response', $response);
    }
}
