<?php

namespace Pyansa\Http\Middleware;

use Cake\Routing\DispatcherFilter;
use Cake\Event\Event;
use Pyansa\Support\Facades\Cors;

class HandleCors extends DispatcherFilter
{
    /**
     * Sobreescritura de Cake\Routing\DispatcherFilter::beforeDispatch
     *
     * @param Cake\Event\Event $event
     * @return void
     */
    public function beforeDispatch(Event $event)
    {
        $cors = Cors::getFacadeRoot();
        $request = $event->data['request'];
        $response = $event->data['response'];

        if (!$cors->isCorsRequest($request)) {
            return;
        } else if ($cors->isPreflightRequest($request)) {
            return $cors->handlePreflightRequest($request, $response);
        } else if (!$cors->isRequestAllowed($request)) {
            return $cors->handleForbiddenRequest($request, $response);
        } else {
            $cors->handleAllowedRequest($request, $response);
        }
    }
}
