<?php

namespace Pyansa\Translation;

use Illuminate\Translation\Translator as IlluminateTranslator;
use Illuminate\Translation\FileLoader;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\LoaderInterface;

/**
 * Esta clase extiende de Illuminate\Translation\Translator para ser usada en vez de la original.
 * Debido a que se requiere agregar traducciones de forma dinamica.
 */
class Translator extends IlluminateTranslator
{
    /**
     * Mezcla lineas de traduccion cargadas
     *
     * @param string $namespace
     * @param string $group
     * @param string $locale
     * @param string $lines
     * @return void
     */
    public function merge($namespace, $group, $locale, $lines)
    {
        if (!$this->isLoaded($namespace, $group, $locale)) {
            $this->load($namespace, $group, $locale);
        }

        $this->loaded[$namespace][$group][$locale] = array_replace_recursive(
            $this->loaded[$namespace][$group][$locale],
            $lines
        );
    }
}
