<?php

namespace Pyansa\Translation;

use Illuminate\Support\ServiceProvider;
use Illuminate\Translation\FileLoader;

class TranslationServiceProvider extends ServiceProvider
{
    /**
     * Registra los providers requeridos para el funcionamiento de este
     *
     * @return void
     */
    protected function registerRequiredProviders()
    {
        $this->app->register('Pyansa\Filesystem\FilesystemServiceProvider');
    }

    /**
     * Registra el fileloader
     *
     * @return void
     */
    protected function registerLoader()
    {
        $this->app->instance('translation.loader', new FileLoader($this->app['files'], ""));
    }

    /**
     * Registra el translator
     *
     * @return void
     */
    protected function registerTranslator()
    {
        $this->app->instance('translator', new Translator($this->app['translation.loader'], "es"));
    }

    /**
     * Sobreescritura de Illuminate\Support\ServiceProvider::register
     * Registra el service provider
     *
     * @return void
     */
    public function register()
    {
        $this->registerRequiredProviders();
        $this->registerLoader();
        $this->registerTranslator();
    }
}
