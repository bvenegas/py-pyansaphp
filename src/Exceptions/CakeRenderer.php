<?php

namespace Pyansa\Exceptions;

use Cake\Error\ExceptionRenderer;

class CakeRenderer extends ExceptionRenderer implements RendererInterface
{
}
