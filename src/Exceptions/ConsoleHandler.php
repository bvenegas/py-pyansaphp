<?php

namespace Pyansa\Exceptions;

use Pyansa\Log\ConsoleReport;
use Pyansa\Support\Facades\Log;
use Cake\Console\ConsoleOutput;

class ConsoleHandler extends Handler
{
    /**
     * Request de la aplicacion
     *
     * @var Cake\Console\ConsoleOutput
     */
    protected $output;

    /**
     * Constructor de la clase
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->output = new ConsoleOutput('php://stderr');
    }

    /**
     * Crea el reporte adecuado
     *
     * @param  Exception  $exception
     * @return Pyansa\Log\Report
     */
    protected function resolveReport($exception)
    {
        if (isset($this->report)) {
            return $this->report;
        }

        $this->report = new ConsoleReport($exception);

        return $this->report;
    }

    /**
     * Crea el renderer adecuado
     *
     * @param  Exception  $exception
     * @return Pyansa\Exceptions\RendererInterface
     */
    protected function resolveRenderer($exception)
    {
        if (isset($this->renderer)) {
            return $this->renderer;
        }

        if ($this->isAjax() || $this->wantsJson()) {
            $renderer = new JsonRenderer($exception, $this->resolveReport($exception), !$this->debug);
        } else {
            $renderer = new CakeRenderer($exception);
        }

        $this->renderer = $renderer;

        return $this->renderer;
    }

    /**
     * Sobreescritura de Pyansa\Exceptions\Handler::report
     * Reporta una excepcion
     *
     * @param  Exception  $exception
     * @return void
     */
    public function report($exception)
    {
        $report = $this->resolveReport($exception);
        Log::error($report->getMessage());
    }

    /**
     * Sobreescritura de Pyansa\Exceptions\Handler::render
     * Renderiza una excepcion
     *
     * @param  Exception  $exception
     * @return Pyansa\Exceptions\RendererInterface
     */
    public function render($exception)
    {
        $renderer = $this->resolveRenderer($exception);
        $message = $renderer->render();
        $this->output->write($message);
    }
}
