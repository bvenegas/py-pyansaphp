<?php

namespace Pyansa\Exceptions;

use Cake\Network\Response;
use Pyansa\Log\HttpReport;
use Pyansa\Support\Facades\ExceptionFormatter;

class JsonRenderer implements RendererInterface
{
    /**
     * Excepcion a renderizar
     *
     * @var Exception
     */
    protected $exception;

    /**
     * Report
     *
     * @var Pyansa\Log\Report
     */
    protected $report;

    /**
     * En caso de true, formatea el mensaje de la excepcion
     *
     * @var boolean
     */
    protected $formatMessage;

    /**
     * Constructor de la clase
     *
     * @param Exception $exception
     * @param Pyansa\Log\HttpReport $report
     * @param boolean $formatMessage
     */
    public function __construct($exception, HttpReport $report, $formatMessage = false)
    {
        $this->formatMessage = $formatMessage;
        $this->exception = $exception;
        $this->report = $report;
    }

    /**
     * Sobreescritura de Pyansa\Exceptions\RenderInterface::render
     * Renderiza la excepcion
     *
     * @return Cake\Network\Response
     */
    public function render()
    {
        $reportId = $this->report->getId();
        if ($this->formatMessage) {
            $message = ExceptionFormatter::getMessage($this->exception);
        } else {
            $message = $this->exception->getMessage();
        }

        if (empty($message)) {
            // en caso de estar vacio el mensaje se usara la clase de la excepcion
            $message = get_class($this->exception);
        }

        if (is_subclass_of($this->exception, 'Exception')) {
            $message = "[LOG_ID: " . $reportId . "] " . $message;
        }

        $data = [
            "success" => false,
            "message" => $message,
            "metadata" => [
                "log_id" => $reportId
            ]
        ];

        $response = new Response();
        $response->type('json');
        $response->body(json_encode($data, JSON_NUMERIC_CHECK));

        return $response;
    }
}
