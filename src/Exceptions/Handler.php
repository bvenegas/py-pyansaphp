<?php

namespace Pyansa\Exceptions;

use ErrorException;
use Cake\Error\FatalErrorException;

/**
 * Esta clase se encarga de manejar los errores/excepciones que puedan presentarse en la aplicacion.
 */
abstract class Handler
{
    /**
     * Configuracion del handler
     *
     * @var array
     */
    protected $config;

    /**
     * Reporte (log) que contiene un id y template customizado a guardar
     *
     * @var Pyansa\Log\Report
     */
    protected $report;

    /**
     * Renderer de la excepcion
     *
     * @var Pyansa\Exceptions\RendererInterface
     */
    protected $renderer;

    /**
     * Indica si la aplicacion esta en modo debug
     *
     * @var boolean
     */
    public $debug;

    /**
     * Determina si se debe reportar la excepcion
     *
     * @var boolean
     */
    public $dontReport = false;

    /**
     * Determina si se debe renderizar la excepcion
     *
     * @var boolean
     */
    public $dontRender = false;

    /**
     * Constructor de la clase
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        $defaults = [
            'errorLevel' => -1
        ];
        $this->config = $config + $defaults;
    }

    /**
     * Reporta una excepcion.
     *
     * @param  Exception  $exception
     * @return void
     */
    abstract public function report($exception);

    /**
     * Renderiza una excepcion
     *
     * @param  Exception  $exception
     * @return mixed
     */
    abstract public function render($exception);

    /**
     * Registra el handler para errores
     *
     * @return void
     */
    protected function registerErrorHandler()
    {
        set_error_handler([$this, 'handleError']);
    }

    /**
     * Registra el handler para excepciones
     *
     * @return void
     */
    protected function registerExceptionHandler()
    {
        set_exception_handler([$this, 'handleUncaughtException']);
    }

    /**
     * Registra el handler para el shutdown
     *
     * @return void
     */
    protected function registerShutdownHandler()
    {
        register_shutdown_function([$this, 'handleShutdown']);
    }

    /**
     * Determina si el tipo de error es fatal.
     *
     * @param  int   $type
     * @return bool
     */
    protected function isFatal($type)
    {
        return in_array($type, [E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_PARSE]);
    }

    /**
     * Limpia el buffer de salida para que la excepcion se renderise correctamente
     *
     * @return void
     */
    protected function clearOutput()
    {
        while (ob_get_level()) {
            ob_end_clean();
        }
    }

    /**
     * Registra el handler en la aplicacion
     *
     * @return void
     */
    public function register()
    {
        error_reporting($this->config['errorLevel']);
        $this->registerErrorHandler();
        $this->registerExceptionHandler();
        $this->registerShutdownHandler();
    }

    /**
     * Maneja un error
     *
     * @param  int     $level
     * @param  string  $message
     * @param  string  $file
     * @param  int     $line
     * @param  array   $context
     * @return void
     * @throws ErrorException
     */
    public function handleError($level, $message, $file = '', $line = 0, $context = [])
    {
        if (error_reporting() & $level) {
            throw new ErrorException($message, 0, $level, $file, $line);
        }
    }

    /**
     * Maneja una excepcion no capturada
     *
     * @param  Exception  $exception
     * @return void
     */
    public function handleUncaughtException($exception)
    {
        $this->handleException($exception);
    }

    /**
     * Maneja el evento shutdown
     *
     * @return void
     */
    public function handleShutdown()
    {
        $error = error_get_last();

        // If an error has occurred that has not been displayed, we will create a fatal
        // error exception instance and pass it into the regular exception handling
        // code so it can be displayed back out to the developer for information.
        if (!is_null($error) && $this->isFatal($error['type'])) {
            extract($error);
            $this->handleException(new FatalErrorException($message, 0, $file, $line));
        }
    }

    /**
     * Maneja una excepcion.
     * Las funciones handlerError, handleUncaughtException y handleshutdown al final tendran que pasar a traves de esta
     * funcion.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function handleException($exception)
    {
        if (!$this->dontReport) {
            $this->report($exception);
        }
        if (!$this->dontRender) {
            $this->render($exception);
        }
    }
}
