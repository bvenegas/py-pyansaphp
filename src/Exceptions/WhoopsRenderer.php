<?php

namespace Pyansa\Exceptions;

use Whoops\Run as Whoops;
use Whoops\Handler\PrettyPageHandler;
use Cake\Network\Response;

class WhoopsRenderer implements RendererInterface
{
    /**
     * Excepcion a renderizar
     *
     * @var Exception
     */
    protected $exception;

    /**
     * Instancia de Whoops
     *
     * @var Whoops\Run
     */
    protected $whoops;

    /**
     * Constructor de la clase
     *
     * @param Exception $exception
     */
    public function __construct($exception)
    {
        $this->exception = $exception;
        $whoops = new Whoops();
        $whoops->pushHandler(new PrettyPageHandler());
        $whoops->writeToOutput(false);
        $whoops->allowQuit(false);
        $this->whoops = $whoops;
    }

    /**
     * Sobreescritura de Pyansa\Exceptions\RenderInterface::render
     * Renderiza la excepcion
     *
     * @return Cake\Network\Response
     */
    public function render()
    {
        $response = new Response();
        $response->body($this->whoops->handleException($this->exception));

        return $response;
    }
}
