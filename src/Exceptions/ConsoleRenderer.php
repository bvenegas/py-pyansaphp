<?php

namespace Pyansa\Exceptions;

class ConsoleRenderer implements RendererInterface
{
    /**
     * Excepcion a renderizar
     *
     * @var Exception
     */
    protected $exception;

    /**
     * Constructor de la clase
     *
     * @param Exception $exception
     */
    public function __construct($exception)
    {
        $this->exception = $exception;
    }

    /**
     * Sobreescritura de Pyansa\Exceptions\RenderInterface::render
     * Renderiza la excepcion
     *
     * @return string
     */
    public function render()
    {
        $exception = $this->exception;
        $message = sprintf(
            "<error>%s</error> %s in [%s, line %s]",
            get_class($exception),
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine()
        );

        return $message;
    }
}
