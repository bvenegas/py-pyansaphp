<?php

namespace Pyansa\Exceptions;

use Illuminate\Support\ServiceProvider;
use Cake\Core\Configure;
use Pyansa\Exceptions\Formatters\Factory;

class ExceptionServiceProvider extends ServiceProvider
{
    /**
     * Registra los providers requeridos para el funcionamiento de este
     *
     * @return void
     */
    protected function registerRequiredProviders()
    {
        $this->app->register('Pyansa\Foundation\Providers\ConfigServiceProvider');
        $this->app->register('Pyansa\Foundation\Providers\FacadesServiceProvider');
        $this->app->register('Pyansa\Log\LogServiceProvider');
    }

    /**
     * Registra la instancia del handler
     *
     * @return void
     */
    protected function registerHandler()
    {
        if ($this->isRunningFromConsole()) {
            $handler = new ConsoleHandler(Configure::read('Error'));
        } else {
            $this->app->register('Pyansa\Http\Cors\CorsServiceProvider');
            $handler = new HttpHandler(Configure::read('Error'));
        }

        $handler->debug = Configure::read('debug');
        $handler->register();
        $this->app->instance('exception', $handler);
    }

    /**
     * Registra la instancia del formatter factory
     *
     * @return void
     */
    protected function registerFormatter()
    {
        $this->app->instance('exception.formatter', new Factory());
    }

    /**
     * Determina si se esta corriendo la aplicacion desde consola
     *
     * @return boolean
     */
    protected function isRunningFromConsole()
    {
        return PHP_SAPI === 'cli';
    }

    /**
     * Sobreescritura de Illuminate\Support\ServiceProvider::register
     * Registra el service provider
     *
     * @return void
     */
    public function register()
    {
        $this->registerRequiredProviders();
        $this->registerFormatter();
        $this->registerHandler();
    }
}
