<?php

namespace Pyansa\Exceptions;

use Cake\Network\Response;
use Pyansa\Log\HttpReport;
use Pyansa\Support\Facades\Cors;
use Pyansa\Support\Facades\Log;
use Pyansa\Support\Facades\Request;

class HttpHandler extends Handler
{
    /**
     * Request de la aplicacion
     *
     * @var Cake\Network\Request
     */
    protected $request;

    /**
     * Constructor de la clase
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->request = Request::getFacadeRoot();
    }

    /**
     * Verifica si el request prefiere un json como respuesta
     *
     * @return boolean
     */
    protected function wantsJson()
    {
        return $this->request->accepts('application/json');
    }

    /**
     * Verifica si el request es mediante ajax
     *
     * @return boolean
     */
    protected function isAjax()
    {
        return $this->request->header('X-Requested-With') === 'XMLHttpRequest';
    }

    /**
     * Crea el reporte adecuado
     *
     * @param  Exception  $exception
     * @return Pyansa\Log\Report
     */
    protected function resolveReport($exception)
    {
        if (isset($this->report)) {
            return $this->report;
        }

        $this->report = new HttpReport($exception, $this->request->query, $this->request->data);

        return $this->report;
    }

    /**
     * Crea el renderer adecuado
     *
     * @param  Exception  $exception
     * @return Pyansa\Exceptions\RendererInterface
     */
    protected function resolveRenderer($exception)
    {
        if (isset($this->renderer)) {
            return $this->renderer;
        }

        if ($this->isAjax() || $this->wantsJson()) {
            $renderer = new JsonRenderer($exception, $this->resolveReport($exception), !$this->debug);
        } else if ($this->debug) {
            // si es debug, se renderiza un response hecho con Whoops
            $renderer = new WhoopsRenderer($exception);
        } else {
            // por default, se renderiza un response de cake
            $renderer = new CakeRenderer($exception);
        }

        $this->renderer = $renderer;

        return $this->renderer;
    }

    /**
     * Sobreescritura de Pyansa\Exceptions\Handler::report
     * Reporta una excepcion
     *
     * @param  Exception  $exception
     * @return void
     */
    public function report($exception)
    {
        $report = $this->resolveReport($exception);
        Log::error($report->getMessage());
    }

    /**
     * Sobreescritura de Pyansa\Exceptions\Handler::render
     * Renderiza una excepcion
     *
     * @param  Exception  $exception
     * @return Pyansa\Exceptions\RendererInterface
     */
    public function render($exception)
    {
        $cors = Cors::getFacadeRoot();
        $request = $this->request;
        $renderer = $this->resolveRenderer($exception);

        if (!$cors->isCorsRequest($request)) {
            return $renderer->render()->send();
        }

        if ($cors->isPreflightRequest($request)) {
            return $cors->handlePreflightRequest($request, new Response())->send();
        }

        if (!$cors->isRequestAllowed($request)) {
            return $cors->handleForbiddenRequest($request, new Response())->send();
        }

        return $cors->handleAllowedRequest($request, $response->render())->send();
    }
}
