<?php

namespace Pyansa\Laravel\Exceptions\Formatters;

/**
 * Formatter encargado de formatear las excepciones del tipo PDOException tratando de omitir informacion sensible del sistema
 */
class PDOExceptionFormatter extends Formatter
{
    /**
     * Mensajes de error de acuerdo al codigo de error
     *
     * @var array
     */
    public static $messages = array(
        "1005" => "Error de base de datos; No se ha podido crear una tabla.",
        "1006" => "Error de base de datos; No se ha podido crear una base de datos.",
        "1007" => "Error de base de datos; La base de datos que intenta crear ya existe.",
        "1008" => "Error de base de datos; La base de datos que intenta eliminar no existe.",
        "1009" => "Error de base de datos; No se ha podido eliminar una base de datos.",
        "1044" => "Error de base de datos; No se tiene acceso a la base de datos.",
        "1045" => "Error de base de datos; Acceso denegado.",
        "1046" => "Error de base de datos; No se ha seleccionado una base de datos.",
        "1048" => "Error de base de datos; La consulta contiene columnas que no pueden ser null.",
        "1049" => "Error de base de datos; La base de datos no existe.",
        "1050" => "Error de base de datos; La tabla que intenta crear ya existe.",
        "1051" => "Error de base de datos; La tabla no existe.",
        "1052" => "Error de base de datos; La consulta contiene columnas ambiguas.",
        "1053" => "Error de base de datos; La base de datos ha dejado de responder.",
        "1054" => "Error de base de datos; La consulta contiene columnas desconocidas.",
        "1058" => "Error de base de datos; La cantidad de columnas no corresponde con la cantidad de valores.",
        "1060" => "Error de base de datos; El nombre de la columna esta duplicado.",
        "1061" => "Error de base de datos; El nombre de la clave esta duplicado.",
        "1062" => "Error de base de datos; Informacion duplicada.",
        "1064" => "Error de base de datos; La consulta es incorrecta.",
        "1169" => "Error de base de datos; No se puede realizar la accion debido a una restricción de información duplicada.",
        "1216" => "Error de base de datos; No se puede agregar o actualizar el registro; no existe el registro con el cual se quiere relacionar.",
        "1217" => "Error de base de datos; No se puede eliminar o actualizar el registro; existen otros registros que dependen de este.",
        "1280" => "Error de base de datos; El nombre del indice es incorrecto.",
        "1317" => "Error de base de datos; La consulta ha sido interrumpida",
        "1365" => "Error de base de datos; Division entre cero",
        "1451" => "Error de base de datos; No se puede eliminar o actualizar el registro; existen otros registros que dependen de este.",
        "1452" => "Error de base de datos; No se puede agregar o actualizar el registro; no existe el registro con el cual se quiere relacionar."
    );

    /**
     * Sobreescritura de Pyansa\Exceptions\Formatters\Formatter::format
     * Formatea el mensaje de la excepcion de acuerdo al codigo de error.
     *
     * @return string|null
     */
    protected function format($exception)
    {
        $errorCode = $exception->errorInfo[1];
        if (isset(static::$messages[$errorCode])) {
            return static::$messages[$errorCode];
        }

        return null;
    }
}
