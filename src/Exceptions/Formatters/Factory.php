<?php

namespace Pyansa\Exceptions\Formatters;

use Exception;

/**
 * Esta clase sirve para crear dinamicamente formatters de excepciones
 */
class Factory
{
    /**
     * Formatters disponibles
     *
     * @var array
     */
    protected $formatters = array(
        "PDOException" => "Pyansa\Exceptions\Formatters\PDOExceptionFormatter"
    );

    /**
     * Registra un nuevo formatter.
     * En caso que el formatter ya este ligado a un tipo de excepcion, este se reemplazara.
     *
     * @param  string $exceptionClass
     * @param  string $formatterClass
     * @return void
     */
    public function register($exceptionClass, $formatterClass)
    {
        $this->formatters[$exceptionClass] = $formatterClass;
    }

    /**
     * Crea un nuevo formatter de acuerdo a la excepcion
     *
     * @param  Exception $exception
     * @return Pyansa\Exceptions\Formatters\Formatter|null
     */
    public function make(Exception $exception)
    {
        $exceptionClass = get_class($exception);
        if (isset($this->formatters[$exceptionClass])) {
            $formatterClass = $this->formatters[$exceptionClass];
            return new $formatterClass($exception);
        }

        return null;
    }
}
