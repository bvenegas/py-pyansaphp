<?php

namespace Pyansa\Exceptions\Formatters;

/**
 * Clase base para los formatters de excepciones.
 * Un Formatter se encarga de encontrar un patron en el mensaje de la excepcion. Si este patron coincide
 * entonces el mensaje de la excepcion es reemplazado por otro. Es de mucha ayuda al querer evitar que se devuelvan
 * mensajes de excepciones con informacion sensible del sistema.
 */
abstract class Formatter
{
    /**
     * Excepcion a formatear
     *
     * @var Exception
     */
    protected $exception;

    /**
     * Constructor de la clase
     *
     * @param Exception $exception
     */
    public function __construct($exception)
    {
        $this->exception = $exception;
    }

    /**
     * Formatea el mensaje de la excepcion
     *
     * @return string
     */
    abstract protected function format($exception);

    /**
     * Obtiene el mensaje formateado o en su defecto el mensaje de la excepcion sin formatear
     *
     * @return string
     */
    public function getMessage()
    {
        $message = $this->format($this->exception);
        if (!is_string($message)) {
            $message = $this->exception->getMessage();
        }

        return $message;
    }

    /**
     * Magic method __toString()
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getMessage();
    }
}
