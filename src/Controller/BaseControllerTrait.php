<?php

namespace Pyansa\Controller;

use Pyansa\Support\Facades\DB;
use Pyansa\Support\Facades\Response;

/**
 * Este trait esta enfocado para ser usado en los controladores base (e.g. AppController).
 * Agregando funciones de facil acceso a otros componentes (conexiones, validators, CORS, etc.)
 */
trait BaseControllerTrait
{
    /**
     * Retorna la conexion default o crea una nueva con los valores de `config`
     *
     * @param  string $name Nomber de la conexion
     * @param  array $config Configuracion de la conexion
     * @param  boolean $isSecure `true` para devolver un boolean en caso de error, `false` no cacha la excepcion
     * @return Cake\Datasource\DataSource
     */
    protected function getConnection($name = "default", $config = null, $secure = false)
    {
        return DB::connection($name, $config, $secure);
    }

    /**
     * Alias para Pyansa\Controller\BaseControllerTrait::getConnection
     *
     * @param  string $name Nomber de la conexion
     * @param  array $config Configuracion de la conexion
     * @param  boolean $isSecure `true` para devolver un boolean en caso de error, `false` no cacha la excepcion
     * @return Cake\Datasource\DataSource
     */
    protected function getConexion($name = "default", $config = null, $secure = false)
    {
        return $this->getConnection($name, $config, $secure);
    }

    /**
     * Se realiza el JSON con los datos a retornar con su debido Content-Type y las propiedades normalmente usadas para ExtJS
     *
     * @param array $data
     * @param integer $options
     * @return Cake\Network\Response
     */
    public function asJson($data, $options = null)
    {
        $data = array_merge(
            [
                "success" => false,
                "message" => "",
                "records" => null,
                "metadata" => null
            ],
            $data
        );

        if ($options === null) {
            $options = JSON_NUMERIC_CHECK;
        }

        $this->response->type('json');
        $this->response->body(json_encode($data, $options));

        return $this->response;
    }
}
