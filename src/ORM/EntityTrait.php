<?php

namespace Pyansa\ORM;

/**
 * Este trait esta enfocado para darle la funcionalidad basica a un entity 
 */
trait EntityTrait
{
    use LazyLoadTrait;
}
