<?php

namespace Pyansa\Database\Expression;

/**
 * Esta clase se encarga de resolver un arreglo al convertir sus elementos a expresiones where que se puedan agregar
 * a una instancia de Cake\Database\Query
 */
class WhereResolver
{
    /**
     * Devuelve una expression simple de comparación.
     *
     * @param  string $property
     * @param  string $operator
     * @param  mixed $value
     * @return array
     */
    protected function getSimpleExpression($property, $operator, $value)
    {
        return [$property . ' ' . $operator => $value];
    }

    /**
     * Devuelve una expression LIKE de comparación.
     *
     * @param  string $property
     * @param  string $operator
     * @param  mixed $value
     * @return array
     */
    protected function getLikeExpression($property, $operator, $value)
    {
        // si el valor no contiene el wildcard % al princio o al final, se agregan en ambos lados
        $pos = strpos($value, '%');
        $value = $pos === 0 || $pos === count($value) - 1 ? $value : '%' . $value . '%';

        return [$property . ' ' . $operator => $value];
    }

    /**
     * Resuelve el arreglo al retornar un arreglo de expressiones de acuerdo al operador proporcionado.
     *
     * @param  array $items
     * @return array
     */
    public function resolve($items)
    {
        $expressions = [];
        $operators = [
            "=" => "=",
            "==" => "=",
            "===" => "=",
            "eq" => "=",
            "!=" => "!=",
            "!==" => "!=",
            "ne" =>"!=",
            "<" => "<",
            "lt" =>"<",
            "<=" => "<=",
            "le" =>"<=",
            ">" => ">",
            "gt" => ">",
            ">=" => ">=",
            "ge" => ">=",
            "in" => "in",
            "notin" => "notin",
            "like" =>"like",
            "notlike" => "notlike"
        ];

        foreach ($items as $where) {
            $property = $where['property'];
            $operator = $operators[$where['operator']];
            $value = $where['value'];

            switch ($operator) {
                case '=':
                case '!=':
                case '<':
                case '<=':
                case '>':
                case '>=':
                    $expression = $this->getSimpleExpression($property, $operator, $value);
                    break;
                case 'in':
                    $expression = $this->getSimpleExpression($property, 'IN', $value);
                    break;
                case 'notin':
                    $expression  = $this->getSimpleExpression($property, 'NOT IN', $value);
                    break;
                case 'like':
                    $expression = $this->getLikeExpression($property, 'LIKE', $value);
                    break;
                case 'notlike':
                    $expression = $this->getLikeExpression($property, 'NOT LIKE', $value);
                    break;
                default:
                    $operator = '=';
                    $expression = $this->getSimpleExpression($property, $operator, $value);
                    break;
            }
            $expressions[] = $expression;
        }

        return $expressions;
    }
}
