<?php

namespace Pyansa\Database\Expression;

/**
 * Esta clase se encarga de resolver un arreglo al convertir sus elementos a expresiones order by que se puedan agregar
 * a una instancia de Cake\Database\Query
 */
class OrderByResolver
{
    /**
     * Resuelve el arreglo al retornar un arreglo de expressiones order by
     *
     * @param  array $items
     * @return array
     */
    public function resolve($items)
    {
        $expressions = [];
        foreach ($items as $order) {
            $property = $order['property'];
            $direction = $order['direction'];
            $expressions[] = [$property => $direction];
        }

        return $expressions;
    }
}
