<?php

namespace Pyansa\Log;

use Pyansa\Support\Debug\Dumper;

class ConsoleReport extends Report
{
    /**
     * Sobreescritura de Pyansa\Log\Report::resolveMessage
     * Resuelve el mensage del log en base a la excepcion
     *
     * @return string
     */
    protected function resolveMessage()
    {
        $tpl = "\n############################### Exception ###############################\n" .
            "Id: {id}\n" .
            "Class: {class}\n" .
            "Message: {message}\n" .
            "File: {file}\n" .
            "Line: {line}\n" .
            "Trace:\n" .
            "{trace}";

        $id = $this->id;
        $class = get_class($this->exception);
        $message = $this->exception->getMessage();
        if (empty($message)) {
            $message = $class;
        }
        $file = $this->exception->getFile();
        $line = $this->exception->getLine();
        $trace = Dumper::toString($this->exception->getTrace());

        return str_replace(
            [
                "{id}",
                "{class}",
                "{message}",
                "{file}",
                "{line}",
                "{trace}"
            ],
            [
                $id,
                $class,
                $message,
                $file,
                $line,
                $trace
            ],
            $tpl
        );
    }
}
