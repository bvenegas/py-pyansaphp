<?php

namespace Pyansa\Log;

use Illuminate\Support\ServiceProvider;
use Monolog\Logger as Monolog;
use Cake\Core\Configure;

class LogServiceProvider extends ServiceProvider
{
    /**
     * Registra los providers requeridos para el funcionamiento de este
     *
     * @return void
     */
    protected function registerRequiredProviders()
    {
        $this->app->register('Pyansa\Foundation\Providers\ConfigServiceProvider');
    }

    /**
     * Registra la instancia del logger en la aplicacion
     *
     * @return void
     */
    protected function registerLogger()
    {
        $path = LOGS . end(Configure::read('Log'))['file'] . ".log";
        $monolog = new Monolog($this->app->environment());
        $writer = new Writer($monolog);
        $writer->useDailyFiles($path, 10);
        $this->app->instance('log', $writer);
        $this->app->bind('Psr\Log\LoggerInterface', function($app) {
            return $app['log']->getMonolog();
        });
    }

    /**
     * Sobreescritura de Illuminate\Support\ServiceProvider::register
     * Registra el service provider
     *
     * @return void
     */
    public function register()
    {
        $this->registerRequiredProviders();
        $this->registerLogger();
    }
}
