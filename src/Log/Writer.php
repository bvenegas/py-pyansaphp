<?php

namespace Pyansa\Log;

use InvalidArgumentException;
use Monolog\Logger as MonologLogger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Formatter\LineFormatter;
use Psr\Log\LoggerInterface as PsrLoggerInterface;
use Illuminate\Contracts\Logging\Log as LogContract;

class Writer implements LogContract, PsrLoggerInterface
{
    /**
     * Configuracion
     *
     * @var array
     */
    protected $config;

    /**
     * Instancia de Monolog
     *
     * @var Monolog\Logger
     */
    protected $monolog;

    /**
     * Niveles de error
     *
     * @var array
     */
    protected $levels = [
        'debug'     => MonologLogger::DEBUG,
        'info'      => MonologLogger::INFO,
        'notice'    => MonologLogger::NOTICE,
        'warning'   => MonologLogger::WARNING,
        'error'     => MonologLogger::ERROR,
        'critical'  => MonologLogger::CRITICAL,
        'alert'     => MonologLogger::ALERT,
        'emergency' => MonologLogger::EMERGENCY,
    ];

    /**
     * Constrcutor de la clase
     *
     * @param  Monolog\Logger  $monolog
     * @return void
     */
    public function __construct(MonologLogger $monolog)
    {
        $this->monolog = $monolog;
    }

    /**
     * Obtiene el formatter default para Monolog
     *
     * @return Monolog\Formatter\LineFormatter
     */
    protected function getDefaultFormatter()
    {
        return new LineFormatter(null, null, true, true);
    }

    /**
     * Escribe un mensaje usando Monolog.
     *
     * @param  string  $level
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    protected function writeLog($level, $message, $context)
    {
        $this->monolog->{$level}($message, $context);
    }

    /**
     * Parsea el level a una constante de Monolog
     *
     * @param  string  $level
     * @return int
     *
     * @throws InvalidArgumentException
     */
    protected function parseLevel($level)
    {
        if (isset($this->levels[$level])) {
            return $this->levels[$level];
        }

        throw new InvalidArgumentException("Invalid log level.");
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Logging\Log::emergency
     * Escribe un log de nivel emergency
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function emergency($message, array $context = array())
    {
        return $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Logging\Log::alert
     * Escribe un log de nivel alert
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function alert($message, array $context = array())
    {
        return $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Logging\Log::critical
     * Escribe un log de nivel critical
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function critical($message, array $context = array())
    {
        return $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Logging\Log::error
     * Escribe un log de nivel error
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function error($message, array $context = array())
    {
        return $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Logging\Log::warning
     * Escribe un log de nivel warning
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function warning($message, array $context = array())
    {
        return $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Logging\Log::notice
     * Escribe un log de nivel notice
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function notice($message, array $context = array())
    {
        return $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Logging\Log::info
     * Escribe un log de nivel info
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function info($message, array $context = array())
    {
        return $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Logging\Log::debug
     * Escribe un log de nivel debug
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function debug($message, array $context = array())
    {
        return $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Logging\Log::log
     * Escribe un log
     *
     * @param  string  $level
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function log($level, $message, array $context = array())
    {
        return $this->writeLog($level, $message, $context);
    }

    /**
     * Pasa dinamicamente las llamadas dentro del writer
     *
     * @param  string  $level
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function write($level, $message, array $context = array())
    {
        return $this->writeLog($level, $message, $context);
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Logging\Log::useFiles
     * Registra un handler de archivos para Monolog
     *
     * @param  string  $path
     * @param  string  $level
     * @return void
     */
    public function useFiles($path, $level = 'debug')
    {
        $this->monolog->pushHandler($handler = new StreamHandler($path, $this->parseLevel($level)));

        $handler->setFormatter($this->getDefaultFormatter());
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Logging\Log::useDailyFiles
     * Registra un handler de archivos diarios para Monolog
     *
     * @param  string  $path
     * @param  int     $days
     * @param  string  $level
     * @return void
     */
    public function useDailyFiles($path, $days = 0, $level = 'debug')
    {
        $this->monolog->pushHandler(
            $handler = new RotatingFileHandler($path, $days, $this->parseLevel($level))
        );

        $handler->setFormatter($this->getDefaultFormatter());
    }

    /**
     * Obtiene la instancia de Monolog
     *
     * @return Monolog\Logger
     */
    public function getMonolog()
    {
        return $this->monolog;
    }

    /**
     * Asigna la config del logger
     *
     * Nota: Esta funcion solo se agrego por compatibilidad con el proceso de bootstrap de cake
     *
     * @param array $config
     * @return void
     */
    public function config($config)
    {
        $this->config = $config;
    }
}
