<?php

namespace Pyansa\Log;

use Pyansa\Support\Debug\Dumper;

class HttpReport extends Report
{
    /**
     * Parametros de GET de la peticion
     *
     * @var array
     */
    protected $paramsGet;

    /**
     * Parametros POST de la peticion
     *
     * @var array
     */
    protected $paramsPost;

    /**
     * Constructor de la clase
     *
     * @param Exception $exception
     */
    public function __construct($exception, array $paramsGet = [], array $paramsPost = [])
    {
        parent::__construct($exception);
        $this->paramsGet = $paramsGet;
        $this->paramsPost = $paramsPost;
    }

    /**
     * Sobreescritura de Pyansa\Log\Report::resolveMessage
     * Resuelve el mensage del log en base a la excepcion
     *
     * @return string
     */
    protected function resolveMessage()
    {
        $tpl = "\n############################### Exception ###############################\n" .
            "Id: {id}\n" .
            "Class: {class}\n" .
            "Message: {message}\n" .
            "File: {file}\n" .
            "Line: {line}\n" .
            "Params (GET):\n" .
            "{paramsGet}\n" .
            "Params (POST):\n" .
            "{paramsPost}\n" .
            "Trace:\n" .
            "{trace}";

        $id = $this->id;
        $class = get_class($this->exception);
        $message = $this->exception->getMessage();
        if (empty($message)) {
            $message = $class;
        }
        $file = $this->exception->getFile();
        $line = $this->exception->getLine();
        $paramsGet = Dumper::toString($this->paramsGet);
        $paramsPost = Dumper::toString($this->paramsPost);
        $trace = Dumper::toString($this->exception->getTrace());

        return str_replace(
            [
                "{id}",
                "{class}",
                "{message}",
                "{file}",
                "{line}",
                "{paramsGet}",
                "{paramsPost}",
                "{trace}"
            ],
            [
                $id,
                $class,
                $message,
                $file,
                $line,
                $paramsGet,
                $paramsPost,
                $trace
            ],
            $tpl
        );
    }
}
