<?php

namespace Pyansa\Log;

use DateTime;

abstract class Report
{
    /**
     * Id para identificar el log
     *
     * @var string
     */
    protected $id;

    /**
     * Excepcion con la que se generara el log
     *
     * @var Exception
     */
    protected $excepcion;

    /**
     * Mensaje del log
     *
     * @var string
     */
    protected $message;

    /**
     * Constructor de la clase
     *
     * @param Exception $exception
     */
    public function __construct($exception)
    {
        $this->exception = $exception;
        $this->id = static::generateId();
    }

    /**
     * Resuelve el mensage del log en base a la excepcion
     *
     * @return string
     */
    abstract protected function resolveMessage();

    /**
     * Retorna el mensaje buildeado del log
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->resolveMessage();
    }

    /**
     * Retorna el id del log
     *
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Genera un id basado en el timestamp del sistema
     *
     * @return string
     */
    protected static function generateId()
    {
        $now = new DateTime();

        return $now->getTimestamp();
    }

    /**
     * Parseo del log a string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getMessage();
    }
}
