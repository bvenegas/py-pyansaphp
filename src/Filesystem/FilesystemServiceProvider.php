<?php

namespace Pyansa\Filesystem;

use Illuminate\Support\ServiceProvider;
use Illuminate\Filesystem\Filesystem;

class FilesystemServiceProvider extends ServiceProvider
{
    /**
     * Registra la instancia de Filesystem
     *
     * @return void
     */
    protected function registerFilesystem()
    {
        $this->app->instance('files', new Filesystem());
    }

    /**
     * Sobreescritura de Illuminate\Support\ServiceProvider::register
     * Registra el service provider
     *
     * @return void
     */
    public function register()
    {
        $this->registerFilesystem();
    }
}
