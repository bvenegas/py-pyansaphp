<?php

namespace Pyansa\Support\Debug;

use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;

class Dumper
{
    /**
     * Dumpea el valor al regresarlo como string con un Symfony\Component\VarDumper\Dumper\CliDumper
     *
     * @param mixed $value
     * @return string
     */
    public static function toString($value)
    {
        $cloner = new VarCloner();
        $dumper = new CliDumper();
        $stream = fopen('php://memory', 'r+b');

        $value = $cloner->cloneVar($value)->getLimitedClone(5, -1);
        $dumper->dump($value, $stream);
        $value = stream_get_contents($stream, -1, 0);
        fclose($stream);

        return $value;
    }

    /**
     * Dumpea el valor al guardarlo como string en un archivo
     *
     * @param mixed $value
     * @param string $file
     * @return void
     */
    public static function toFile($value, $file)
    {
        file_put_contents($file, static::toString($value));
    }
}
