<?php

namespace Pyansa\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Facade para el filesystem de la aplicacion
 */
class File extends Facade
{
    /**
     * Sobreescritura de Illuminate\Support\Facades\Facade::getFacadeAccessor
     * Obtiene el nombre con el que se registro el componente
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'files';
    }
}
