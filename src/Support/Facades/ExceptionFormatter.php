<?php

namespace Pyansa\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Facade para el formateo de excepciones usando un Pyansa\Exceptions\Formatters\Formatter
 */
class ExceptionFormatter extends Facade
{
    /**
     * Sobreescritura de Illuminate\Support\Facades\Facade::getFacadeAccessor
     * Obtiene el nombre con el que se registro el componente
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'exception.formatter';
    }

    /**
     * Obtiene el mensage formateado de la excepcion al crear el formatter correspondiente
     *
     * @param  Exception $exception
     * @return string
     */
    public static function getMessage($exception)
    {
        $formatter = static::make($exception);
        if (!is_null($formatter)) {
            return $formatter->getMessage();
        }

        return $exception->getMessage();
    }

    /**
     * Alias de la funcion getMessage
     *
     * @param Exception $exception
     * @return string
     */
    public static function format($exception)
    {
        return static::getMessage($exception);
    }
}
