<?php

namespace Pyansa\Support\Facades;

use Illuminate\Support\Facades\Facade;
use Cake\Datasource\ConnectionManager;
use Exception;

/**
 * Facade para la aplicacion
 */
class DB extends Facade
{
    /**
     * Ejecuta una query en la conexion default
     *
     * @param string $query
     * @param array $params
     * @param array $types
     * @return Cake\Database\StatementInterface|array|number
     */
    public static function query($query, array $params = [], array $types = [])
    {
        return static::connection()->query($query, $params, $types);
    }

    /**
     * Retorna la conexion default o crea una nueva con los valores de `config`
     *
     * @param  string $name Nomber de la conexion
     * @param  array $config Configuracion de la conexion
     * @param  boolean $isSecure `true` para devolver un boolean en caso de error, `false` no cacha la excepcion
     * @return Cake\Datasource\DataSource
     */
    public static function connection($name = "default", $config = null, $secure = false)
    {
        try {
            if (empty($config)) {
                return static::get($name);
            } else {
                // crea una nueva conexion con config
                static::config(
                    $name,
                    [
                        'className' => 'Pyansa\Database\Connection',
                        'driver' => 'Cake\Database\Driver\Mysql',
                        'persistent' => false,
                        'host' => $config['host'],
                        'username' => $config['username'],
                        'password' => $config['password'],
                        'database' => $config['database'],
                        'encoding' => 'utf8',
                        'cacheMetadata' => true,
                        'quoteIdentifiers' => false,
                        'log' => false,
                    ]
                );
                return static::get($name);
            }
        } catch (Exception $ex) {
            if ($secure) {
                return null;
            } else {
                throw $ex;
            }
        }
    }

    /**
     * Maneja las llamadas pasandolas de forma dinamica a Cake\Datasource\ConnectionManager
     *
     * @param  string  $method
     * @param  array   $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        switch (count($args))
        {
            case 0:
                return ConnectionManager::$method();

            case 1:
                return ConnectionManager::$method($args[0]);

            case 2:
                return ConnectionManager::$method($args[0], $args[1]);

            case 3:
                return ConnectionManager::$method($args[0], $args[1], $args[2]);

            case 4:
                return ConnectionManager::$method($args[0], $args[1], $args[2], $args[3]);

            default:
                return forward_static_call_array(['Cake\Datasource\ConnectionManager', $method], $args);
        }
    }
}
