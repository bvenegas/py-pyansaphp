<?php

namespace Pyansa\Support\Facades;

use Illuminate\Support\Facades\Facade;
use Pyansa\Database\Expression\WhereResolver;
use Pyansa\Database\Expression\OrderByResolver;

/**
 * Facade para resolver expresiones de una query
 */
class ExpressionResolver extends Facade
{
    /**
     * Resuelve las expressiones where
     *
     * @param  array $items
     * @return array
     */
    public static function where($items)
    {
        $resolver = new WhereResolver();

        return $resolver->resolve($items);
    }

    /**
     * Resuelve las expressiones orderBy
     *
     * @param  array $items
     * @return array
     */
    public static function orderBy($items)
    {
        $resolver = new OrderByResolver();

        return $resolver->resolve($items);
    }
}
