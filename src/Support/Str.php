<?php

namespace Pyansa\Support;

class Str
{
    /**
     * Realiza un trim al valor ademas de reducir los espacios en blancos entre palabras a solo uno.
     *
     * @param string $value
     * @return string
     */
    public static function reduceSpaces($value)
    {
        $value = trim($value);
        $value = preg_replace('!\s+!', ' ', $value);

        return $value;
    }
}
