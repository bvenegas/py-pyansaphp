<?php

namespace Pyansa\Routing;

use Cake\Network\Response;

class ResponseFactory
{
    /**
     * Sobreescritura de Illuminate\Contracts\Routing\ResponseFactory::make
     * Retorna un response
     *
     * @param string $content
     * @param integer $status
     * @param array $headers
     * @return Cake\Network\Response
     */
    public function make($content = '', $status = 200, array $headers = array())
    {
        $response = new Response();
        $response->body($content);
        $response->statusCode($status);
        $response->header($headers);

        return $response;
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Routing\ResponseFactory::make
     * Retorna un response JSON
     *
     * @param  string|array  $data
     * @param  int    $status
     * @param  array  $headers
     * @param  int    $options
     * @return Cake\Network\Response
     */
    public function json($data = array(), $status = 200, array $headers = array(), $options = 0)
    {
        if (is_array($data)) {
            $data = json_encode($data, $options);
        }

        $response = $this->make($data, $status, $headers);
        $response->type('json');

        return $response;
    }
}
