<?php

namespace Pyansa\Routing;

use Illuminate\Support\ServiceProvider;

class RoutingServiceProvider extends ServiceProvider
{
    /**
     * Registra el factory de los validators
     *
     * @return void
     */
    protected function registerFactory()
    {
        $this->app->instance('Pyansa\Routing\ResponseFactory', new ResponseFactory());
    }

    /**
     * Sobreescritura de Illuminate\Support\ServiceProvider::register
     * Registra el service provider
     *
     * @return void
     */
    public function register()
    {
        $this->registerFactory();
    }
}
