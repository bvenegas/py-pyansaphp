<?php

namespace Pyansa\Foundation;

use Illuminate\Container\Container;
use Illuminate\Contracts\Foundation\Application as ApplicationContract;

/**
 * Esta clase es una version simple de una aplicacion basada en la clase Illuminate\Foundation\Application
 */
class Application extends Container implements ApplicationContract
{
    /**
     * Version de la libreria
     *
     * @var string
     */
    const VERSION = '3.1.13';

    /**
     * Ruta base de la aplicacion
     *
     * @var string
     */
    protected $basePath;

    /**
     * Todos los service provers registrados
     *
     * @var array
     */
    protected $serviceProviders = [];

    /**
     * Los nombres de los service providers cargados
     *
     * @var array
     */
    protected $loadedProviders = [];

    /**
     * Indica si la aplicacion ha sido "booted"
     *
     * @var bool
     */
    protected $booted = false;

    /**
     * El array de booting callbacks.
     *
     * @var array
     */
    protected $bootingCallbacks = [];

    /**
     * El array de booted callbacks.
     *
     * @var array
     */
    protected $bootedCallbacks = [];

    /**
     * Constructor de la clase.
     *
     * @param  string|null  $basePath
     * @return void
     */
    public function __construct($basePath = null)
    {
        $this->registerBaseBindings();
        if ($basePath) {
            $this->setBasePath($basePath);
        }
    }

    /**
     * Registra los bindings basicos en el container
     *
     * @return void
     */
    protected function registerBaseBindings()
    {
        static::setInstance($this);
        $this->instance('app', $this);
        $this->instance('Illuminate\Container\Container', $this);
    }


    /**
     * Marca el provider proporcionado como registrado
     *
     * @param  Illuminate\Support\ServiceProvider
     * @return void
     */
    protected function markAsRegistered($provider)
    {
        $class = get_class($provider);
        $this->serviceProviders[] = $provider;
        $this->loadedProviders[$class] = true;
    }

    /**
     * Bootea el service provider
     *
     * @param  Illuminate\Support\ServiceProvider  $provider
     * @return void
     */
    protected function bootProvider(ServiceProvider $provider)
    {
        if (method_exists($provider, 'boot'))
        {
            return $this->call([$provider, 'boot']);
        }
    }

    /**
     * Llama los booting callbacks de la aplicacion
     *
     * @param  array  $callbacks
     * @return void
     */
    protected function fireAppCallbacks(array $callbacks)
    {
        foreach ($callbacks as $callback)
        {
            call_user_func($callback, $this);
        }
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Foundation\Application::version
     * Obtiene la version de la aplicacion
     *
     * @return string
     */
    public function version()
    {
        return static::VERSION;
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Foundation\Application::basePath
     * Obtiene la ruta base de la aplicacion
     *
     * @param  string  $path
     * @return string
     */
    public function basePath()
    {
        return $this->basePath;
    }

    /**
     * Asigna la ruta base de la aplicacion
     *
     * @param  string  $basePath
     * @return $this
     */
    public function setBasePath($basePath)
    {
        $this->basePath = $basePath;

        return $this;
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Foundation\Application::environment
     * Obtiene o checa el environment de la aplicacion
     *
     * @param  mixed
     * @return string
     */
    public function environment()
    {
        if (func_num_args() > 0) {
            $patterns = is_array(func_get_arg(0)) ? func_get_arg(0) : func_get_args();
            foreach ($patterns as $pattern) {
                if (str_is($pattern, $this['env'])) {
                    return true;
                }
            }
            return false;
        }
        return $this['env'];
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Foundation\Application::isDownForMaintenance
     * Determina si la aplicacion esta en mantenimiento
     *
     * @return bool
     */
    public function isDownForMaintenance()
    {
        // Nunca esta en mantenimiento
        return false;
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Foundation\Application::registerConfiguredProviders
     * Registra todos los providers configurados
     *
     * @return void
     */
    public function registerConfiguredProviders()
    {
        // No se registra ningun provider
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Foundation\Application::register
     * Registra un service provider en la aplicacion
     *
     * @param  Illuminate\Support\ServiceProvider|string  $provider
     * @param  array  $options
     * @param  bool   $force
     * @return Illuminate\Support\ServiceProvider
     */
    public function register($provider, $options = array(), $force = false)
    {
        if ($registered = $this->getProvider($provider) && !$force) {
            return $registered;
        }
        // If the given "provider" is a string, we will resolve it, passing in the
        // application instance automatically for the developer. This is simply
        // a more convenient way of specifying your service provider classes.
        if (is_string($provider)) {
            $provider = $this->resolveProviderClass($provider);
        }
        $provider->register();
        // Once we have registered the service we will iterate through the options
        // and set each of them on the application so they will be available on
        // the actual loading of the service objects and for developer usage.
        foreach ($options as $key => $value) {
            $this[$key] = $value;
        }
        $this->markAsRegistered($provider);
        // If the application has already booted, we will call this boot method on
        // the provider class so it has an opportunity to do its boot logic and
        // will be ready for any usage by the developer's application logics.
        if ($this->booted) {
            $this->bootProvider($provider);
        }

        return $provider;
    }

    /**
     * Obtiene la instancia del service provider registrado si existe
     *
     * @param  Illuminate\Support\ServiceProvider|string  $provider
     * @return Illuminate\Support\ServiceProvider|null
     */
    public function getProvider($provider)
    {
        $name = is_string($provider) ? $provider : get_class($provider);
        return array_first($this->serviceProviders, function($key, $value) use ($name)
        {
            return $value instanceof $name;
        });
    }

    /**
     * Resuelve una instancia service provider apartir de su clase
     *
     * @param  string  $provider
     * @return Illuminate\Support\ServiceProvider
     */
    public function resolveProviderClass($provider)
    {
        return new $provider($this);
    }

    /**
     * Registra un provider y service deferred
     *
     * @param  string  $provider
     * @param  string  $service
     * @return void
     */
    public function registerDeferredProvider($provider, $service = null)
    {
        // Once the provider that provides the deferred service has been registered we
        // will remove it from our local list of the deferred services with related
        // providers so that this container does not try to resolve it out again.
        if ($service) {
            unset($this->deferredServices[$service]);
        }
        $this->register($instance = new $provider($this));
        if (!$this->booted) {
            $this->booting(function() use ($instance)
            {
                $this->bootProvider($instance);
            });
        }
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Foundation\Application::boot
     * Bootea los service providers de la aplicacion
     *
     * @return void
     */
    public function boot()
    {
        if ($this->booted) {
            return;
        }
        // Once the application has booted we will also fire some "booted" callbacks
        // for any listeners that need to do work after this initial booting gets
        // finished. This is useful when ordering the boot-up processes we run.
        $this->fireAppCallbacks($this->bootingCallbacks);
        array_walk($this->serviceProviders, function($p) {
            $this->bootProvider($p);
        });
        $this->booted = true;
        $this->fireAppCallbacks($this->bootedCallbacks);
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Foundation\Application::booting
     * Registra un boot listener.
     *
     * @param  mixed  $callback
     * @return void
     */
    public function booting($callback)
    {
        $this->bootingCallbacks[] = $callback;
    }

    /**
     * Sobreescritura de Illuminate\Contracts\Foundation\Application::booted
     * Registra un "booted" listener.
     *
     * @param  mixed  $callback
     * @return void
     */
    public function booted($callback)
    {
        $this->bootedCallbacks[] = $callback;
        if ($this->isBooted()) {
            $this->fireAppCallbacks(array($callback));
        }
    }

    /**
     * Determina si la aplicacion ha sido booted
     *
     * @return bool
     */
    public function isBooted()
    {
        return $this->booted;
    }
}
