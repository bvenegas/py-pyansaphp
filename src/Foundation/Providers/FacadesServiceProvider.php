<?php

namespace Pyansa\Foundation\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Facade;

class FacadesServiceProvider extends ServiceProvider
{
    /**
     * Prepara los facades
     *
     * @return void
     */
    protected function prepareFacades()
    {
        Facade::clearResolvedInstances();
        Facade::setFacadeApplication($this->app);
    }

    /**
     * Sobreescritura de Illuminate\Support\ServiceProvider::register
     * Registra el service provider
     *
     * @return void
     */
    public function register()
    {
        $this->prepareFacades();
    }
}
