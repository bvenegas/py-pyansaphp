<?php

namespace Pyansa\Foundation\Providers;

use Illuminate\Support\ServiceProvider;
use Cake\Routing\DispatcherFactory;
use Pyansa\Http\Middleware\SetDispatchedRequestResponse;

class HttpBootServiceProvider extends ServiceProvider
{
    /**
     * Registra los providers requeridos para el funcionamiento de este
     *
     * @return void
     */
    protected function registerRequiredProviders()
    {
        $this->app->register('Pyansa\Exceptions\ExceptionServiceProvider');
        $this->app->register('Pyansa\Foundation\Providers\ConfigServiceProvider');
        $this->app->register('Pyansa\Foundation\Providers\FacadesServiceProvider');
        $this->app->register('Pyansa\Log\LogServiceProvider');
        $this->app->register('Pyansa\Filesystem\FilesystemServiceProvider');
        $this->app->register('Pyansa\Translation\TranslationServiceProvider');
        $this->app->register('Pyansa\Validation\ValidationServiceProvider');
        $this->app->register('Pyansa\Http\Cors\CorsServiceProvider');
        $this->app->register('Pyansa\Routing\RoutingServiceProvider');
    }

    /**
     * Agrega un before dispatch filter para obtener las instancias de Request y Response
     * que la aplicacion crea
     *
     * @return void
     */
    protected function registerMiddlewares()
    {
        DispatcherFactory::add(new SetDispatchedRequestResponse($this->app));
    }

    /**
     * Sobreescritura de Illuminate\Support\ServiceProvider::register
     * Registra el service provider
     *
     * @return void
     */
    public function register()
    {
        $this->registerRequiredProviders();
        $this->registerMiddlewares();
    }
}
