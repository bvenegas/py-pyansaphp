<?php

namespace Pyansa\Foundation\Providers;

use Illuminate\Support\ServiceProvider;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;

class ConfigServiceProvider extends ServiceProvider
{
    /**
     * En caso de no estar cargada la configuracion esta se carga
     *
     * @return void
     */
    protected function loadConfig()
    {
        $config = Configure::read();
        if (count($config) <= 1) {
            // por default la config tiene el elemento "debug" aunque no este cargada,
            // por eso se compara la cantidad de elementos que tiene para saber si reamelnte se ha cargado
            Configure::config('default', new PhpConfig());
            Configure::load('app', 'default', false);
        }

        $this->app['env'] = Configure::read('debug') ? 'debug' : 'production';
    }

    /**
     * Sobreescritura de Illuminate\Support\ServiceProvider::register
     * Registra el service provider
     *
     * @return void
     */
    public function register()
    {
        $this->loadConfig();
    }
}
